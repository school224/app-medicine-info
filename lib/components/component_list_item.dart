import 'dart:math';

import 'package:app_api_test2/model/medicine_list_item.dart';
import 'package:flutter/material.dart';


class ComponentListItem extends StatelessWidget {
  const ComponentListItem({super.key, required this.item, required this.callback});

  final MedicineListItem item;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
          color: Colors.white.withOpacity(.70),
          border: Border.all(width: 5, color: Colors.primaries[Random().nextInt(Colors.primaries.length)])
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text('${item.CPNT_CD == null ? '의약품 코드가 없습니다.' : item.CPNT_CD}',
            style: TextStyle(
              color: Colors.black,
              fontSize: 18,
              fontWeight: FontWeight.bold,
              fontStyle: FontStyle.italic
            ),),
            Text('의약품 한글명 = ${item.DRUG_CPNT_KOR_NM == null ? '의약품명이 없습니다.' : item.DRUG_CPNT_KOR_NM}'),
            Text('의약품 영문명 = ${item.DRUG_CPNT_ENG_NM == null ? '없습니다.' : item.DRUG_CPNT_ENG_NM}'),
            Text('제형코드 = ${item.FOML_CD == null ? '제형 코드가 없습니다.' : item.FOML_CD}'),
            Text('제형명 = ${item.FOML_NM == null ? '제형 명이 없습니다.' : item.FOML_NM}'),
            Text('투여경로 = ${item.DOSAGE_ROUTE_CODE == null ? '투여경로가 없습니다.' : item.DOSAGE_ROUTE_CODE}'),
            Text('투여단위 = ${item.DAY_MAX_DOSG_QY_UNIT == null ? '투여단위가 없습니다.' : item.DAY_MAX_DOSG_QY_UNIT}'),
            Text('1일최대투여량 = ${item.DAY_MAX_DOSG_QY == null ? '1일 최대 투여량 값이 없습니다.' : item.DAY_MAX_DOSG_QY}'),
          ],
        ),
      ),
    );
  }
}
