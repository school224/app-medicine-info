import 'package:app_api_test2/config/config_api.dart';
import 'package:app_api_test2/model/medicine_response.dart';
import 'package:dio/dio.dart';

import '../model/medicine_list.dart';

class RepoMedicineList {
  final String _baseUrl = '$apiDrug/1471000/DayMaxDosgQyByIngdService/getDayMaxDosgQyByIngdInq';

  Future<MedicineList> getList({int page = 1, String CPNT_CD = '', String DRUG_CPNT_KOR_NM = '', String DRUG_CPNT_ENG_NM = ''}) async {
    Map<String, dynamic> params = {};
    params['pageNo'] = page;
    params['serviceKey'] = Uri.encodeFull(apiDrugKey);
    params['type'] = 'json';
    if (CPNT_CD != '') params['CPNT_CD'] = Uri.encodeFull(CPNT_CD);
    if (DRUG_CPNT_KOR_NM != '') params['DRUG_CPNT_KOR_NM'] = Uri.encodeFull(DRUG_CPNT_KOR_NM);
    if (DRUG_CPNT_ENG_NM != '') params['DRUG_CPNT_ENG_NM'] = Uri.encodeFull(DRUG_CPNT_ENG_NM);


    Dio dio = Dio();

    final response = await dio.get(
      _baseUrl,
      queryParameters: params,
      options: Options(
        followRedirects: false,
      ),
    );

    MedicineResponse resultModel = MedicineResponse.fromJson(response.data);
    return resultModel.body!;
  }
}