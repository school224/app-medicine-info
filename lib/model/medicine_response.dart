import 'package:app_api_test2/model/medicine_list.dart';

class MedicineResponse {
  MedicineList? body;

  MedicineResponse({this.body});

  factory MedicineResponse.fromJson(Map<String, dynamic> json) {
    return MedicineResponse(
      body: json['body'] == null ? null : MedicineList.fromJson(json['body']),
    );
  }
}