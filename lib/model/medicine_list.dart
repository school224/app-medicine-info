import 'medicine_list_item.dart';

class MedicineList {
  int currentPage;
  int totalPage;
  int totalItemCount;
  List<MedicineListItem> items;

  MedicineList (
      this.currentPage,
      this.totalPage,
      this.totalItemCount,
      this.items
      );

  factory MedicineList.fromJson(Map<String, dynamic> json) {
    int totalItemCount = json['totalCount'] == null ? 0 : json['totalCount'] as int;
    int numOfRows = json['numOfRows'] == null ? 10 : json['numOfRows'] as int;
    int totalPage = (totalItemCount / numOfRows).ceil();

    return MedicineList(
      json['pageNo'] == null ? 1 : json['pageNo'] as int, //공공 API 에서 'pageNo' 이 필수값이 아니므로 factory 내부에서 강제값을 준다
      (json['totalCount'] != null && json['numOfRows'] != null) ? totalPage : 1,
      json['totalCount'] == null ? 1 : json['totalCount'] as int,
      json['items'] == null ? [] : (json['items'] as List).map((e) => MedicineListItem.fromJson(e)).toList()
    );
  }
}