class MedicineListItem {
  String CPNT_CD;
  String DRUG_CPNT_KOR_NM;
  String DRUG_CPNT_ENG_NM;
  String FOML_CD;
  String FOML_NM;
  String DOSAGE_ROUTE_CODE;
  String DAY_MAX_DOSG_QY_UNIT;
  String DAY_MAX_DOSG_QY;

  MedicineListItem(this.CPNT_CD,
      this.DRUG_CPNT_KOR_NM,
      this.DRUG_CPNT_ENG_NM,
      this.FOML_CD,
      this.FOML_NM,
      this.DOSAGE_ROUTE_CODE,
      this.DAY_MAX_DOSG_QY_UNIT,
      this.DAY_MAX_DOSG_QY);

  factory MedicineListItem.fromJson(Map<String, dynamic> json) {
    return MedicineListItem(
      json['CPNT_CD'] == null ? '' : json['CPNT_CD'],
      json['DRUG_CPNT_KOR_NM'] == null ? '' : json['DRUG_CPNT_KOR_NM'],
      json['DRUG_CPNT_ENG_NM'] == null ? '' : json['DRUG_CPNT_ENG_NM'],
      json['FOML_CD'] == null ? '' : json['FOML_CD'],
      json['FOML_NM'] == null ? '' : json['FOML_NM'],
      json['DOSAGE_ROUTE_CODE'] == null ? '' : json['DOSAGE_ROUTE_CODE'],
      json['DAY_MAX_DOSG_QY_UNIT'] == null ? '' : json['DAY_MAX_DOSG_QY_UNIT'],
      json['DAY_MAX_DOSG_QY'] == null ? '' : json['DAY_MAX_DOSG_QY'],
    );
  }
}