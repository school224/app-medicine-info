import 'package:app_api_test2/components/component_appbar_filter.dart';
import 'package:app_api_test2/components/component_count_title.dart';
import 'package:app_api_test2/components/component_custom_loading.dart';
import 'package:app_api_test2/components/component_list_item.dart';
import 'package:app_api_test2/components/component_no_contents.dart';
import 'package:app_api_test2/pages/page_search.dart';
import 'package:app_api_test2/repository/repo_medicine_list.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

import '../model/medicine_list_item.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> with AutomaticKeepAliveClientMixin{
  final _scrollController = ScrollController();

  List<MedicineListItem> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  String _CPNT_CD = '';
  String _DRUG_CPNT_KOR_NM = '';
  String _DRUG_CPNT_ENG_NM = '';

  @override
  void initState() {
    super.initState();

    _scrollController.addListener(() {
      if (_scrollController.offset == _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });

    _loadItems();
  }

  Future<void> _loadItems({bool reFresh = false}) async {
    if (reFresh) {
      _list = [];
      _currentPage = 1;
      _totalPage = 1;
      _totalItemCount = 0;
    }

    if (_currentPage <= _totalPage) {

      BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
        return ComponentCustomLoading(cancelFunc: cancelFunc);
      });

      await RepoMedicineList()
          .getList(page: _currentPage,  CPNT_CD: _CPNT_CD, DRUG_CPNT_KOR_NM: _DRUG_CPNT_KOR_NM, DRUG_CPNT_ENG_NM: _DRUG_CPNT_ENG_NM)
          .then((res) {
              BotToast.closeAllLoading();

              setState(() {
                _totalPage = res.totalPage;
                _totalItemCount = res.totalItemCount;

                _list = [..._list, ...res.items];

                _currentPage++;
        });
      }).catchError((err) => BotToast.closeAllLoading());
    }
    
    if (reFresh) {
      _scrollController.animateTo(0, duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
    }
  }


  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/back9.gif"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: ComponentAppbarFilter(
              title: '1일 의약품 투여정보',
              actionIcon: Icons.search,
              callback: () async {
                final searchResult = await Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PageSearch(
                          CPNT_CD: _CPNT_CD,
                          DRUG_CPNT_KOR_NM: _DRUG_CPNT_KOR_NM,
                          DRUG_CPNT_ENG_NM: _DRUG_CPNT_ENG_NM
                        )));

              if (searchResult != null && searchResult[0]) {
                _CPNT_CD = searchResult[1];
                _DRUG_CPNT_KOR_NM = searchResult[2];
                _DRUG_CPNT_ENG_NM = searchResult[3];
                _loadItems(reFresh: true);
              }
            },
          ),
          body: ListView(
            controller: _scrollController,
            children: [
              ComponentCountTitle(icon: Icons.medication, count: _totalItemCount, unitName: '건', itemName: '의약품 정보'),
              _buildBody(),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildBody() {
    if (_totalItemCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentListItem(item: _list[index], callback: () {}),
          ),
        ],
      );
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height -50 -20,
        child: const ComponentNoContents(icon: Icons.medication, msg: '의약품 정보가 없습니다.'),
      );
    }
  }

  @override
  bool get wantKeepAlive => true;
}
