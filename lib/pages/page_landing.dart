import 'dart:async';

import 'package:app_api_test2/pages/page_index.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PageLanding extends StatefulWidget {
  const PageLanding({Key? key}) : super(key: key);

  @override
  State<PageLanding> createState() => _PageLandingState();
}

class _PageLandingState extends State<PageLanding> {
  @override
  void initState() {
    Timer(Duration(seconds: 3), () {
      Get.offAll(PageIndex());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image.asset('assets/g6.gif', fit: BoxFit.cover,),
          ),
          CircularProgressIndicator()
        ],
      ),
    );
  }
}