import 'dart:ui';

import 'package:app_api_test2/components/component_appbar_popup.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class PageSearch extends StatefulWidget {
  PageSearch({super.key, this.CPNT_CD = '', this.DRUG_CPNT_KOR_NM = '', this.DRUG_CPNT_ENG_NM = ''});

  String CPNT_CD;
  String DRUG_CPNT_KOR_NM;
  String DRUG_CPNT_ENG_NM;

  @override
  State<PageSearch> createState() => _PageSearchState();
}

class _PageSearchState extends State<PageSearch> {
  final GlobalKey<FormBuilderState> _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
          title: '의약품 상세 검색',
      ),
      body: _buildBody(),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 40,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: Colors.pink
            ),
            onPressed: () {
              String inputCPNT_CD = _formKey.currentState!.fields['CPNT_CD']!.value;
              String inputDRUG_CPNT_KOR_NM = _formKey.currentState!.fields['DRUG_CPNT_KOR_NM']!.value;
              String inputDRUG_CPNT_ENG_NM = _formKey.currentState!.fields['DRUG_CPNT_ENG_NM']!.value;

              Navigator.pop(
                context,
                [true, inputCPNT_CD, inputDRUG_CPNT_KOR_NM, inputDRUG_CPNT_ENG_NM]
              );
            },
            child: const Text('검색하기'),
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return ListView(
      children: [
        FormBuilder(
          key: _formKey,
          autovalidateMode: AutovalidateMode.disabled,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(height: 15,),
              FormBuilderTextField(
                name: 'CPNT_CD',
                initialValue: widget.CPNT_CD,
                maxLines: null,
                keyboardType: TextInputType.multiline,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: EdgeInsets.all(20),
                  icon: Icon(Icons.question_mark),
                  hintText: '성분코드를 입력하세요',
                  labelText: '성분코드',
                ),
              ),
              Container(height: 15,),
              FormBuilderTextField(
                name: 'DRUG_CPNT_KOR_NM',
                initialValue: widget.DRUG_CPNT_KOR_NM,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: EdgeInsets.all(20),
                  icon: Icon(Icons.question_mark),
                  hintText: '성분명(한글명)을 입력하세요',
                  labelText: '성분명(한글)',
                ),
              ),
              Container(height: 15,),
              FormBuilderTextField(
                name: 'DRUG_CPNT_ENG_NM',
                initialValue: widget.DRUG_CPNT_ENG_NM,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: EdgeInsets.all(20),
                  icon: Icon(Icons.question_mark),
                  hintText: '성분명(영문명 대소문자에 유의)을 입력하세요',
                  labelText: '성분명(영문)',
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
